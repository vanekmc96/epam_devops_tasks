#!/bin/bash

# Input File Path
FILEPATH=$1
# Folder of input and output files
DIRNAME=`dirname $FILEPATH`
# Full path of output file
NEWFILEPATH="$DIRNAME/accounts_new.csv"

# Saving header to new file
head -n 1 $FILEPATH > $NEWFILEPATH

# Formating values in loop
while IFS="," read -r id location name title email department
do
    # Making first letter of the word as uppercase
    # Firstname. Getting first word
    firstname=${name%% *}
    firstname=${firstname,,}
    # Lastname. Getting last word
    lastname=${name##* }
    lastname=${lastname,,}
    # Name with appercase each word
    name=`echo $name | sed -E "s/(\w)(\w*)/\U\1\L\2/g"`
    
    # If name occure more then one time, location will be added to email address 
    if [[ $(cat accounts.csv | grep -iE "${firstname:0:1}\w* ${lastname}" | wc -l) -gt 1 ]]
    then
        email="${firstname:0:1}${lastname}$location@abc.com"
    else
        email="${firstname:0:1}${lastname}@abc.com"
    fi

    # output result line to new file. Replacing | to ,
    echo "$id,$location,$name,${title//|/,},$email,$department" >> $NEWFILEPATH

done < <(tail -n +2 $FILEPATH | sed -r 's/(".*),(.*")/\1|\2/g' ) # input file with replacing comma in double brackets with | 

