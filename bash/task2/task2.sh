#!/bin/bash

# Input File Path
FILEPATH=$1
# Folder of input and output files
DIRNAME=`dirname $FILEPATH`
# Full path of output file
NEWFILEPATH="$DIRNAME/output.json"

# Function converts json header
header(){
    testName=`echo "$@" | cut -d ',' -f 1 | sed -r 's/\[\s|\s\]//g'`
    echo '{' > $NEWFILEPATH
    echo "  \"testName\": \"$testName\"," >> $NEWFILEPATH
    echo "  \"tests\": [" >> $NEWFILEPATH
    echo "    {" >> $NEWFILEPATH
}

# Function converts json tests
tests(){

    if [[ "$@" =~ "not ok" ]]
    then
        status=false
    else
        status=true
    fi

    name=`echo "$@" | sed -r -e 's/(not )?ok [0-9]+ //' -e 's/, [0-9]+ms$//'`
    duration=`echo $@ | grep -Eo '[0-9]*ms'`

    
    
    if [[ $first -ne 1 ]]
    then
        echo "    }," >> $NEWFILEPATH
        echo "    {" >> $NEWFILEPATH
    fi

    echo "      \"name\": \"$name\"," >> $NEWFILEPATH
    echo "      \"status\": $status," >> $NEWFILEPATH    
    echo "      \"duration\": \"$duration\"" >> $NEWFILEPATH
}

# Function converts json results
summary(){

    success=`echo $@ | cut -d ' ' -f 1`
    failed=`echo $@ | cut -d ' ' -f 6`
    rating=`echo $line | cut -d ' ' -f 11 | tr -d '[%,]'`
    duration=`echo $@ | grep -Eo '[0-9]*ms'`

    echo "    }" >> $NEWFILEPATH
    echo "  ]," >> $NEWFILEPATH
    echo "  \"summary\": {" >> $NEWFILEPATH
    echo "    \"success\": $success," >> $NEWFILEPATH
    echo "    \"failed\": $failed," >> $NEWFILEPATH
    echo "    \"rating\": $rating," >> $NEWFILEPATH
    echo "    \"duration\": \"$duration\"" >> $NEWFILEPATH
    echo "  }" >> $NEWFILEPATH
    echo "}" >> $NEWFILEPATH
}

# Flag for first test input without comma
first=1
# Reading each line in loop. Modified: read last line even if it don't have newline symbol
while IFS= read -r line || [ -n "$line" ]  
do
    # Executing necessary method or skipping line
    case $line in 
        -*) 
	    continue 
            ;;
        [*)
            header $line 
            ;;
        ok* | "not ok"*)
            tests $line 
            first=0
            ;;
        *"tests passed,"*) 
            summary $line
            ;;
    esac

done < $FILEPATH
