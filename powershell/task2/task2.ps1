<#
.SYNOPSIS
    A short one-line action-based description, e.g. 'Tests if a function is valid'
.DESCRIPTION
    A longer description of the function, its purpose, common use cases, etc.
.NOTES
    Information or caveats about the function e.g. 'This function is not supported in Linux'
.LINK
    Specify a URI to a help page, this will show when Get-Help -Online is used.
.EXAMPLE
    Test-MyTestFunction -Verbose
    Explanation of the function or its result. You can include multiple examples with additional .EXAMPLE lines
#>
[CmdletBinding()]
param (
    [Parameter()]
    [ValidateNotNullOrEmpty()]
    # Validate that provided value is csv file
    [ValidateScript(
        {
             if(Import-Csv $_){
                $true
             }else {
                $false
             }
        },
    # Custom error message for invalid
    ErrorMessage = "Please use valid path of csv file"
    )]
    [string]
    # fail if value is not specified
    $OldFile=$(throw "argument is mandatory, please provide a value." )
)

$NewFile = "$((Get-Item $OldFile).DirectoryName)\accounts_new.csv"

$CSV = Import-Csv $OldFile

for ($i = 0; $i -lt $CSV.Count; $i++) {
    # Makes uppercase each word in sentense
    $CSV[$i].name = [cultureinfo]::GetCultureInfo("en-US").TextInfo.ToTitleCase($CSV[$i].name)
    
    # Get first letter of firstname
    $FirstLetterOfName = $CSV[$i].name.split(' ')[0][0].ToString().ToLower()
    # Get lastname (surname) which is acutally last word in sentense
    $Surname = $CSV[$i].name.split(' ')[-1].toLower()
    # Regex pattern for match similar names  
    $EmailPattern = "$FirstLetterOfName\w+\s$Surname"
    # If pattern matches more then once, then add location id to email
    if (($CSV.name -match $EmailPattern).Count -gt 1) {
        $Id = $CSV[$i].location_id
    }else{
        $Id = ''
    }
    # Set Email address
    $CSV[$i].email = "$FirstLetterOfName$Surname$Id@abc.com"
    # $CSV[$i].email = "$($CSV[$i].name.split(' ')[0][0].ToString().ToLower())$($CSV[$i].name.split(' ')[1].toLower())$( ($CSV.Where({$_.name -eq $CSV[$i].name}).Count) -gt 1 ? $CSV[$i].location_id : '')@abc.com"
}

$CSV | Export-Csv $NewFile -Force