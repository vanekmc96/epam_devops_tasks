<#
.SYNOPSIS
    Check if two IP addresses are in the same network by mask
.DESCRIPTION
    Check if two IP addresses are in the same network by mask. Accept ip_address_1, ip_address_2 and network_mask parameters
.EXAMPLE
    Example 1:
    PS> ./task1.ps1 -ip_address_1 192.168.0.5 -ip_address_2 192.168.0.254 -network_mask 24 
     Yes
    Example 2: 
    PS> ./task1.ps1 192.168.1.5 192.168.0.254 24
     No
#>
[CmdletBinding()]
param (
    [Parameter()]
    [ValidateNotNullOrEmpty()]
    # Validate pattern IP address regex
    [ValidatePattern('(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])',
    # Custom error message for invalid
    ErrorMessage = "Please enter ip_address_1 in format XXX.XXX.XXX.XXX"
    )]
    [string]
    # fail if value is not specified
    $ip_address_1=$(throw "ip_address_1 is mandatory, please provide a value." ),
    [Parameter()]
    [ValidateNotNullOrEmpty()]
    [ValidatePattern('(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])',
    ErrorMessage = "Please enter ip_address_2 in format XXX.XXX.XXX.XXX"
    )]
    [string]
    $ip_address_2=$(throw "ip_address_2 is mandatory, please provide a value." ),
    [Parameter()]   
    [ValidateNotNullOrEmpty()]
    # Script block to check mask for valid format (e.g. 25 or 255.255.255.128)
    [ValidateScript(
        {
            $PossibleMasks = foreach ($length in 0..32) {
                $MaskBinary = ('1' * $Length).PadRight(32, '0')
                $DottedMaskBinary = $MaskBinary -replace '(.{8}(?!\z))', '${1}.'
                $SubnetMask = ($DottedMaskBinary.Split('.') | foreach { [Convert]::ToInt32($_, 2) }) -join '.'
                $SubnetMask
            }
            if ($_ -in $possibleMasks -or $_ -in 0..32) {
                $true
            }else{
                $false
            }
        },
        ErrorMessage = "Format or mask is invalid. Please enter network_mask in valid format XXX.XXX.XXX.XXX or XX (e.g. 255.255.128.0 or 17)"
    )]
    [string]
    $network_mask=$(throw "network_mask is mandatory, please provide a value." )
)

# Save values as ipaddress class
[ipaddress]$ip_address_1 = $ip_address_1
[ipaddress]$ip_address_2 = $ip_address_2

# Format mask and save as IPaddress if int provided, otherwise save as ipaddress
if ($network_mask -as [int]){
    $MaskBinary = ('1' * $network_mask).PadRight(32, '0')
    $DottedMaskBinary = $MaskBinary -replace '(.{8}(?!\z))', '${1}.'
    [ipaddress]$mask = ($DottedMaskBinary.Split('.') | foreach { [Convert]::ToInt32($_, 2) }) -join '.'
}elseif ($network_mask -as [ipaddress]) {
    [ipaddress]$mask = $network_mask
}

# Get network IP by bitwise and of IP and mask and compare. If the same output Yes, else No.
if (($ip_address_1.address -band $mask.address) -eq ($ip_address_2.address -band $mask.address)){
    "Yes"
}else{
    "NO"
}
